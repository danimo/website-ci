---
comments: false
date: 2017-03-30
showDate: false
showPagination: false
showSocial: false
showTags: false
title: Über uns
showMap: true
---

Ein Ort im Kiez, der sich für Austausch, gegenseitige Schulung und gute Laune eignet. So soll der xHain werden.

**Tagsüber:** Ein Lernort für Projekt- und Schülergruppen.

**Abends:** Ein Treffpunkt für Hacker\*innen und Bastler\*innen. 

**Zwischendurch:** Ein Tagungsraum oder temporärer Schreibtisch für bürolose Friedrichshainer\*innen. 

So oder ähnlich könnte sich der xHain, wenn es nach Gründer Felix geht, entwickeln.

## Die Nutzer*innen
Wir möchten mit dem xHain einen Ort schaffen, an dem sich Bastler\*innen, Technikinteressierte und (Gesellschafts-)hacker\*innen wohl fühlen.
Damit sind nicht nur Berufstätige, sondern auch Schüler\*innen, Senior\*innen, Menschen in der Ausbildung oder jene die erwerbslos sind gemeint.

Der xHain ist ein rauchfreier Ort. Wer rauchen will, kann das draußen tun.

Diskriminierungsformen aller Art werden bei uns nicht geduldet und führen zum sofortigem Auschluss!
Wer also sexistisch, homophob, rassistisch, antisemitisch, rechtsextrem, altersdikriminierend, antimuslimisch, antiziganisch, chauvinistisch, heterosexistisch, ableistisch, klassistisch oder lookistisch ist braucht nicht zu kommen!



## Barrierefreiheit

Menschen mit handicaps bzw. disabilities sind uns willkommen! Wir haben noch kein rollstuhlgerechtes WC, aber schon eine mobile Rampe und unsere Werkbank ist unterfahrbar für Rollstühle. Wir bemühen uns stetig , weitere Barrieren abzubauen.

<i class="fab fa-accessible-icon fa-5x"></i>



## Adresse

Grünberger Str. 16
10243 Berlin