---
comments: false
date: 2017-04-04
showDate: false
showPagination: false
showSocial: false
showTags: false
title: Impressum
heroImage: /img/gespraechunterbaeumen.jpg
---

## Angaben gemäß § 5 Telemediengesetz (TMG)
xHain hack+makespace</br>
Felix Just</br>
Grünberger Str. 16</br>
10243 Berlin</br>
Deutschland

## Kontakt
[info@x-hain.de](mailto:info@x-hain.de)

## Verantwortlicher für den Inhalt ist gemäß § 55 Abs. 2 Rundfunkstaatsvertrag (RStV)
Felix Just</br>
Grünberger Str. 16</br>
10243 Berlin</br>
Deutschland

## Ausschluss der Haftung

### 1. Haftung für Inhalte
Der Inhalt unserer Internetseite wurde mit größtmöglicher Sorgfalt erstellt. Wir übernehmen jedoch keine Gewähr dafür, dass dieser Inhalt richtig, vollständig, und aktuell ist und zudem noch gefällt. Gemäß § 7 Abs. 1 TMG sind wir für den Inhalt verantwortlich, selbst wenn dieser wurde bestellt.

Gemäß den §§ 8, 9 und 10 TMG ist für uns keine Verpflichtung gegeben, dass wir Informationen von Dritten, die übermittelt oder gespeichert wurden, überwachen oder Umstände erheben, die Hinweise auf nicht rechtmäßige Tätigkeiten ergeben.

Davon nicht berührt, ist unsere Verpflichtung zur Sperrung oder Entfernung von Informationen, welche von den allgemeinen Gesetzen herrührt und denen Beachtung gebührt.

Wir haften allerdings erst in dem Moment, in dem wir von einer konkreten Verletzung von Rechten Kenntnis bekommen. Dann wird eine unverzügliche Entfernung des entsprechenden Inhalts vorgenommen.

### 2. Haftung für Links
Unsere Internetseite enthält Links, die zu externen Internetseiten von Dritten führen, auf deren Inhalte wir jedoch keinen Einfluss haben. Es ist uns daher nicht möglich, eine Gewähr für diese Inhalte zu tragen.

Die Verantwortung dafür hat immer der jeweilige Anbieter/Betreiber der entsprechenden Internetseite. Wir überprüfen die von uns verlinkten Internetseiten zum Zeitpunkt der Verlinkung auf einen möglichen Rechtsverstoß in voller Breite.

Es kann uns jedoch, ohne einen konkreten Anhaltspunkt, nicht zugemutet werden, ständig die verlinkten Internetseiten inhaltlich zu überwachen. Wenn wir jedoch von einer Rechtsverletzung Kenntnis erlangen, werden wir den entsprechenden Link unverzüglich entfernen, das können wir machen.

### 3. Urheberrecht
Die auf unserer Internetseite enthaltenen Inhalte sind, soweit möglich, urheberrechtlich geschützt. Es bedarf einer schriftlichen Genehmigung des Erstellers für denjenigen, der die Inhalte vervielfältigt, bearbeitet, verbreitet oder nützt.

Das Herunterladen und Kopieren unserer Internetseite ist sowohl für den privaten als auch den kommerziellen Gebrauch von uns schriftlich zu gestatten. Wir weisen darauf hin, dass wir hinsichtlich der Inhalte auf unserer Internetseite, soweit sie nicht von uns erstellt worden sind, das Urheberrecht von Dritten jederzeit beachtet hatten.

Wenn Sie uns mitteilen würden, dass Sie trotzdem eine Urheberrechtsverletzung gefunden haben, würden wir das sehr schätzen. Dann können wir den entsprechenden Inhalt sofort entfernen und würden damit das Urheberrecht nicht mehr verletzen, einen rechtswidrigen Zustand also nicht fortsetzen.

<sub>Muster-Impressum von der Flegl Rechtsanwälte GmbH</sub>

