---
title: "WTF are Cryptocurrencies?"
slug: ""
thumbnailImage: /img/cryptocurrency.jpg
heroImage: /img/cryptocurrency.jpg
heroCaption: "(http://community-currency.info/) ([CC BY NC SA](https://creativecommons.org/publicdomain/zero/1.0/deed.de))"
date: 2017-07-03
categories:
- news
- events
tags:
- xhain
- events
keywords:
- xhain
type: blog
aliases:
- /de/2017/07/wtf-cryptocurrency
---

Wir wollen einen Einblick geben in die Welt der Crypto-Währungen.
In einem kurzen Vortrag wird es einen kleinen Rundumschlag zu den folgenden Punkten geben:

1. Was ist die Grundlage moderner Cryptocurrencies? Was ist eine Blockchain?
2. Bitcoin, Ethereum und Zcash.
	- Was sind die Gemeinsamkeiten? 
	- Worin unterscheiden sie sich?
	- Was sind die jeweiligen Kerneigenschaften?
3. Proof of Work versus Proof of Stake

Und da es unwahrscheinlich ist, dass danach keine fragen mehr offen sind, wird es Gelegenheit geben sich weiter auszutauschen und zu diskutieren.

Los geht's am Freitag, den 7. Juli, um 19:30 Uhr.
