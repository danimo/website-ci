---
title: "Die Daten, die ich rief"
slug: ""
thumbnailImage: /img/kattascha.jpg
heroImage: /img/kattascha.jpg
heroCaption: ""
date: 2018-10-24
categories:
- news
- events
tags:
- xhain
- events
keywords:
- xhain
type: blog
aliases:
- /de/2018/10/die-daten-die-ich-rief

---

"Die Daten, die ich rief"

Katharina Nocun hat für ihre Buchrecherche ihre Daten bei zahlreichen
Unternehmen, wie Amazon oder oder Netflix, angefordert und auswerten
lassen. Dabei hat sie mehr über sich selbst erfahren, als ihr lieb war.
Eine unterhaltsame Lesung mit Erlebnisbericht und Anleitung zur
Datenschutz-Selbsthilfe.

Los geht's am Sonntag, den 4.11.2018, um 20:00 Uhr. Eintritt frei!

<!--more-->

Katharina Nocun ist Bürgerrechtlerin, Netzaktivistin und studierte
Ökonomin. Sie leitete bundesweite Kampagnen zum Thema Datenschutz,
Whistleblowing und Bürgerrechte, u.a. für die Bürgerbewegung Campact
e.V., Mehr Demokratie e.V., und den Verbraucherzentrale Bundesverband
(vzbv). 
Sie bloggt unter kattascha.de. 2018 erschien ihr Buch "Die
Daten, die ich rief" bei Lübbe.