---
title: "hackspace statt Angst - Eröffnung am Freitag, den 13. Mai 2016"
slug: ""
thumbnailImage: /img/opening.jpg
heroImage: /img/opening.jpg
heroCaption: "Finn Hackshaw via [Unsplash](https://unsplash.com/photos/FQgI8AD-BSg) ([CC0](https://creativecommons.org/publicdomain/zero/1.0/deed.de))"
date: 2016-04-18
categories:
- news
- events
tags:
- xhain
- events
keywords:
- xhain
type: blog
aliases:
- /de/2016/04/eroeffnungsfeier
---

Am Freitag, den 13. Mai feiern wir die Eröffnung des xHains!  

Ab 18 Uhr steht Euch die Tür offen.

Kommt vorbei und schaut euch an was aus dem kahlen Raum geworden ist und kassiert einen Aufkleber ein.

Musik und Getränke gibt es natürlich auch.

<!--more-->

Um am Freitag die Eröffnung des xHains ordentlich feiern zu können, ist noch einiges zu tun.
Schaut doch mal in’s Pad, vielleicht habt ihr ja Zeit und Lust ein wenig zu helfen:

[To-Do-Liste](http://pad.okfn.org/p/xHain "To-Do-Liste")

Wenn es Freiwillige gibt, bitte

1.  Fragezeichen durch Namen austauschen und danach
2.  kurze Mail an Felix (x-hain@posteo.de) oder per Twitter an @xHain_Hackspace.

Danke für eure Hilfe! <3
