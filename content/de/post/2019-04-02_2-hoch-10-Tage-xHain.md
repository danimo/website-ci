---
title: 2 hoch 10 plus x Tage xHain
thumbnailImage: /img/3rd_birthday.jpg
heroImage: /img/3rd_birthday.jpg
slug: ""
heroCaption: ""
date: 2019-04-02
categories:
- news
- events
tags:
- xhain
type: blog
aliases:
- /de/2019/04/2-hoch-10-plus-x-tage-xhain/
---
Hallo alle miteinander!

Die Zeit rast und der Xhain nähert sich seinem 3-jährigen Bestehen. Das ist auf jeden Fall ein Grund zum Anstoßen.

Allerdings wollen wir nicht bis Mai warten und laden Euch alle herzlich zu unserer "2 hoch 10 plus x Tage xHain"-Festivität. 

Wann? Am 5.4.19 (ja, schon übermorgen) ab 20h

Wo? Natürlich im xHain, Grünberger Str. 14

Kommt vorbei, bringt Freunde mit! Es gibt Musik, Getränke, Laser, Plotter und vieles mehr!
