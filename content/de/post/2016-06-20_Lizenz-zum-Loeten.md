---
title: "Lizenz zum Löten"
slug: ""
thumbnailImage: /img/solder.jpg
heroImage: /img/solder.jpg
heroCaption: "Ossewa via [wikipedia](https://de.wikipedia.org/wiki/Datei:Solder_Paste_Printed_on_a_PCB.jpg) ([CC BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/deed.de))"
date: 2016-06-20
categories:
- news
- events
tags:
- xhain
- events
keywords:
- xhain
type: blog
aliases:
- /de/2016/06/lizenz-zum-loeten
---

Abgerissene Stecker, kaputte Lichterketten, zerfallende Arduino-Projekte, Geräte mit Batteriefach statt Netzteil… Du
solltest mal endlich löten lernen? Hier ist die Gelegenheit! Bring Deine defekten Geräte, die elektronischen Kuscheltiere, das batteriebetriebene Spielzeug, Deine Breadboard-Projekte, (gern auch defekte) Bauteile und Platinen mit, wir lernen löten!

Am Sonntag, den 26. Juni 2016 von 12-15 Uhr im xhain Hack- und Makespace

<!--more-->
Um <a href="mailto:x-hain@posteo.de">Anmeldung</a> wird gebeten.

Unkostenbeitrag 7.50, für SupportFlatrate-Abonnenten 5.- (für den Betrieb und Erhalt des xHains).
Material kann bei Bedarf auch vor Ort erworben werden.
