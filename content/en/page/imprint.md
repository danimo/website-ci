---
comments: false
date: 2017-04-04
showDate: false
showPagination: false
showSocial: false
showTags: false
title: Imprint
heroImage: /img/gespraechunterbaeumen.jpg
---

## According to § 5 Telemediengesetz (TMG)
xHain hack+makespace</br>
Felix Just</br>
Grünberger Str. 14</br>
10243 Berlin</br>
Deutschland

## Contact
[info@x-hain.de](mailto:info@x-hain.de)

## Responsible for the content is according to § 55 Abs. 2 Rundfunkstaatsvertrag (RStV)
Felix Just</br>
Grünberger Str. 14</br>
10243 Berlin</br>
Deutschland


## Exclusion of liability

### 1. Liability for contents
The content of our website was created with the greatest possible care. However, we do not guarantee that this content is correct, complete, up to date or pleasing. According to § 7 Abs. 1 TMG we are responsible for the content, even if it has been ordered.

In accordance with §§ 8, 9 and 10 TMG, we are under no obligation to monitor information transmitted or stored by third parties or to investigate circumstances that indicate unlawful activities.

Not affected by this is our obligation to block or remove information which derives from general laws and which deserves attention.

However, we are not liable until we become aware of a concrete violation of rights. We will then immediately remove the relevant content.

### 2. Liability for links
Our website contains links to external websites of third parties. However, we have no influence on their content. It is therefore not possible for us to guarantee these contents.

The responsibility for this always lies with the respective provider/operator of the corresponding website. We check the Internet pages linked by us at the time of linking for a possible violation of law in full width.

However, we cannot be expected to constantly monitor the content of the linked websites without a concrete indication. However, if we become aware of a violation of the law, we will immediately remove the corresponding link, which we can do.

### 3. Copyright
The contents of our website are, as far as possible, protected by copyright. A written permission of the creator is required for the person who copies, edits, distributes or uses the contents.

Downloading and copying our website is to be permitted by us in writing for both private and commercial use. We would like to point out that we have at all times observed the copyright of third parties with regard to the contents of our website, insofar as they have not been created by us.

If you would inform us that you have nevertheless found a copyright infringement, we would greatly appreciate it. We would then be able to remove the corresponding content immediately and would no longer infringe the copyright, i.e. not continue an illegal situation.


<sub>Translated with www.DeepL.com/Translator </br> Muster-Impressum von der Flegl Rechtsanwälte GmbH</sub>