---
title: "hackspace > fear – Opening on Friday, 13 May 2016"
slug: ""
thumbnailImage: /img/opening.jpg
heroImage: /img/opening.jpg
heroCaption: "Finn Hackshaw via [Unsplash](https://unsplash.com/photos/FQgI8AD-BSg) ([CC0](https://creativecommons.org/publicdomain/zero/1.0/deed.de))"
date: 2016-04-18
categories:
- news
- events
tags:
- xhain
- events
keywords:
- xhain
type: blog
---

We're celebrating the opening of xHain on Friday, 13 May!

Doors open at 6pm.  

Come over, have a look at what we've made of the once bare room and snag a sticker!  

Music and drinks are available.
