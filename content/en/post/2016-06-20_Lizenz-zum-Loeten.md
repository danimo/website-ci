---
title: "License to solder"
slug: ""
thumbnailImage: /img/solder.jpg
heroImage: /img/solder.jpg
heroCaption: "Ossewa via [wikipedia](https://de.wikipedia.org/wiki/Datei:Solder_Paste_Printed_on_a_PCB.jpg) ([CC BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/deed.de))"
date: 2016-06-20
categories:
- news
- events
tags:
- xhain
- events
keywords:
- xhain
type: blog
---

Torn plugs, broken lights , disintegrating Arduino projects , devices with battery compartment instead of power supply ... you might like to finally learn soldering ? Here is your chance ! Bring your defective equipment, electronic stuffed animals , battery operated toys, your breadboard projects, components and circuit boards, we learn !

On Sunday , June 26, 2016 12-15 PM at the xHain hack+makespace

<!--more-->
<a href="mailto:x-hain@posteo.de"> registration </a> would be nice.

Costs: 7.50, for Support Flatrate subscribers 5.- ( for the operation and maintenance of the xHain ) .
If needed material can be purchased locally.
